package de.supaadev.qsg.utils;

import lombok.Getter;
import lombok.Setter;

public class Data {

    @Getter
    @Setter
    private static GameState gameState;

    @Getter
    @Setter
    public static int lobbyCD = 60;

    @Getter
    @Setter
    public static int lobbyTask = 0;

}
