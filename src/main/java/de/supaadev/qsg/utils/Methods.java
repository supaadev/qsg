package de.supaadev.qsg.utils;

import de.supaadev.qsg.manager.LocationManager;
import lombok.Getter;

public class Methods {

    @Getter  private LocationManager locationManager;

    public Methods() {
        init();
    }

    private void init() {
        locationManager = new LocationManager();
    }
}
