package de.supaadev.qsg.utils;

public enum GameState {

    LOBBY, SCHUTZZEIT, INGAME, DEATHMATCH, RESTART;
}
