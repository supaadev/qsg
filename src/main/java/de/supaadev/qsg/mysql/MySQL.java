package de.supaadev.qsg.mysql;


import de.supaadev.qsg.Qsg;
import org.bukkit.Bukkit;
import java.sql.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

public class MySQL {

    private final ExecutorService service;

    private DataBaseCredential credential;

    private Connection connection;

    public MySQL(final DataBaseCredential dataBaseCredential ) {
        this.credential = dataBaseCredential;
        this.service = Executors.newCachedThreadPool( );
    }

    public void connect( ) {
        try {
            connection = DriverManager.getConnection( "jdbc:mysql://" + credential.getHost( ) + ":" + credential.getPort( ) + "/"
                    + credential.getDatabase( ) + "?autoReconnect=true", credential.getUsername( ), credential.getPassword( ) );
            Bukkit.getConsoleSender().sendMessage( Qsg.PREFIX + "Verbindung zur Datenbank aufgebaut" );
        } catch ( SQLException e ) {
            Bukkit.getConsoleSender().sendMessage( Qsg.PREFIX + "Verbindung zur Datenbank gescheitert!" );
        }
    }

    public void createTable( ) {
        try {
            connection.createStatement( ).executeUpdate(
                    "CREATE TABLE IF NOT EXISTS Sumo_stats (NAME VARCHAR(100), UUID VARCHAR(100), " +
                            "PLAYEDGAMES VARCHAR(100), WONGAMES VARCHAR(100), KILLS VARCHAR(100))" );
            Bukkit.getConsoleSender().sendMessage( Qsg.PREFIX + "Tabellen erstellt!" );
        } catch ( SQLException e ) {
            Bukkit.getConsoleSender().sendMessage( Qsg.PREFIX + "Tabellen konnten nicht erstellt werden!" );
        }
    }

    public void close( ) {
        if ( connection != null ) {
            try {
                connection.close( );
                Bukkit.getConsoleSender().sendMessage( Qsg.PREFIX + "Verbindung zur Datenbank geschlossen" );
            } catch ( SQLException e ) {
                e.printStackTrace( );
                Bukkit.getConsoleSender().sendMessage( Qsg.PREFIX + "Verbindung zur Datenbank konnte nicht geschlossen werden" );
            }
        }
    }

    public void update( final String query ) {
        service.execute( () -> {
            try {
                final Statement statement = connection.createStatement( );
                statement.executeUpdate( query );
                statement.close( );
            } catch ( SQLException e ) {
                e.printStackTrace( );
            }
        } );
    }

    public void getResult( final String query, final Consumer<ResultSet> consumer ) {
        service.execute( () -> {
            ResultSet resultSet = null;

            try {
                Statement statement = connection.createStatement( );
                resultSet = statement.executeQuery( query );
            } catch ( SQLException e ) {
                e.printStackTrace();
            }

            consumer.accept( resultSet );
        } );
    }

}
