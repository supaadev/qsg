package de.supaadev.qsg.mysql;


import lombok.Getter;
import org.bukkit.entity.Player;

import java.sql.SQLException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

@Getter
public class DataBaseHandler {

    private final MySQL mySQL;

    protected final ConcurrentHashMap<UUID, QsgStats>  statsCache = new ConcurrentHashMap<>(  );

    public DataBaseHandler( MySQL mySQL ) {
        this.mySQL = mySQL;
    }

    public void loadPlayerIntoCache( final UUID uuid, final Consumer<QsgStats> consumer ) {
        getMySQL().getResult( "SELECT * FROM Sumo_stats WHERE UUID='" + uuid.toString() + "'", resultSet -> {
            final QsgStats qsgStats = new QsgStats( 0, 0,0 );

            try {
                if(resultSet.next()) {
                    qsgStats.setPlayedGames( resultSet.getInt( "PLAYEDGAMES" ) );
                    qsgStats.setWonGames( resultSet.getInt( "WONGAMES" ) );
                    qsgStats.setGlobalKills( resultSet.getInt( "KILLS" ) );
                }
                consumer.accept( qsgStats );
                statsCache.put( uuid, qsgStats );
            } catch ( SQLException e ) {
                e.printStackTrace( );
            }
        } );
    }

    public void loadPlayerIntoCache(final UUID uuid) {
        loadPlayerIntoCache( uuid, sumoStats -> {
        } );
    }

    public void insertPlayerIfNotExists(final Player player) {
        existsPlayer( player.getUniqueId(), exists -> {
            if(!exists) {
                getMySQL( ).update( "INSERT INTO Sumo_stats (NAME, UUID, PLAYEDGAMES, WONGAMES, KILLS) VALUES (" +
                        "'" + player.getName( ) + "', '" + player.getUniqueId( ).toString( ) + "', '" + 0 + "'," +
                        " '" + 0 + "', '" + 0 + "')" );
            }
        } );
    }

    public void existsPlayer( final UUID uuid, final Consumer<Boolean> consumer ) {
        getMySQL().getResult( "SELECT * FROM Sumo_stats WHERE UUID='" + uuid.toString() + "'", resultSet -> {
            try {
                consumer.accept( resultSet.next() );
            } catch ( SQLException e ) {
                consumer.accept( false );
                e.printStackTrace( );
            }
        });
    }

    public void updatePlayerIntoDatabase(final Player player) {
        if(!statsCache.containsKey( player.getUniqueId() ))
            return;

        getMySQL().getResult( "SELECT * FROM Sumo_stats WHERE UUID='" + player.getUniqueId().toString() + "'", resultSet -> {
            try {
                final QsgStats sumoStats = statsCache.get( player.getUniqueId() );

                if(resultSet.next()) {
                    if(!resultSet.getString( "NAME" ).equalsIgnoreCase( player.getName() )) {
                        getMySQL( ).update( "UPDATE Sumo_stats SET NAME= '" + player.getName( )
                                + "' WHERE UUID= '" + player.getUniqueId( ).toString( ) + "';" );
                    }

                    getMySQL( ).update( "UPDATE Sumo_stats SET PLAYEDGAMES= '" + sumoStats.getPlayedGames()
                            + "' WHERE UUID= '" + player.getUniqueId().toString() + "';" );
                    getMySQL( ).update( "UPDATE Sumo_stats SET WONGAMES= '" + sumoStats.getWonGames()
                            + "' WHERE UUID= '" + player.getUniqueId().toString() + "';" );
                    getMySQL( ).update( "UPDATE Sumo_stats SET KILLS= '" + sumoStats.getGlobalKills()
                            + "' WHERE UUID= '" + player.getUniqueId().toString() + "';" );
                } else {
                    getMySQL( ).update( "INSERT INTO Sumo_stats (NAME, UUID, PLAYEDGAMES, WONGAMES, KILLS) VALUES (" +
                            "'" + player.getName( ) + "', '" + player.getUniqueId( ).toString( ) + "', '" + sumoStats.getPlayedGames() + "'," +
                            " '" + sumoStats.getWonGames() + "', '" + sumoStats.getGlobalKills() + "')" );
                }
            } catch ( SQLException e ) {
                e.printStackTrace( );
            }
        } );
    }

    public QsgStats getStats(final UUID uuid) {
        if(!statsCache.containsKey( uuid ))
            return null;

        return statsCache.get( uuid );
    }

    public void getUUID(final String name, final Consumer<UUID> consumer) {
        getMySQL().getResult( "SELECT * FROM Sumo_stats WHERE NAME='" + name + "'", resultSet -> {
            try {
                if( resultSet.next() )
                    consumer.accept( UUID.fromString( resultSet.getString( "UUID" ) ) );
            } catch ( SQLException e ) {
                consumer.accept( null );
            }
            consumer.accept( null );
        } );
    }
}
