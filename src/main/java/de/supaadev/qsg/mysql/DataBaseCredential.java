package de.supaadev.qsg.mysql;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Setter
@Getter
public class DataBaseCredential {

    private String host;
    private int port;
    private String database;
    private String username;
    private String password;

    public static DataBaseCredential getDefault() {
        return new DataBaseCredential( "localhost", 3306, "Sumo", "Sumo", "passwort" );
    }

}
