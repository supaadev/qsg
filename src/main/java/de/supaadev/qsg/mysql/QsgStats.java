package de.supaadev.qsg.mysql;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class QsgStats {

    private int playedGames;
    private int wonGames;
    private int globalKills;

}
