package de.supaadev.qsg.mysql;

import com.google.gson.GsonBuilder;
import lombok.Getter;
import lombok.Setter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@Getter
@Setter
public class Configuration {

    private DataBaseCredential dataBaseCredential;

    public Configuration( ) {
        this.dataBaseCredential = DataBaseCredential.getDefault();
    }

    public void save( final File file ) throws IOException {
        final FileWriter fileWriter = new FileWriter( file );
        fileWriter.write( new GsonBuilder().setPrettyPrinting().create().toJson( this ) );
        fileWriter.flush();
        fileWriter.close();
    }

}
