package de.supaadev.qsg.commands;

import de.supaadev.qsg.QSG;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.FileNotFoundException;

public class SetupCMD implements CommandExecutor {

    private QSG plugin;

    public SetupCMD(QSG plugin) {
        this.plugin = plugin;
        this.plugin.getCommand("setup").setExecutor(this::onCommand);
    }

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if(!(cs instanceof Player)) return true;

        Player player = (Player) cs;


        if(cmd.getName().equalsIgnoreCase("setup")) {
            switch (args.length) {
                case 0:
                    player.sendMessage("");
                    player.sendMessage("");
                    break;
                case 1:
                    if(args[0].equalsIgnoreCase("setlobby")) {
                        try {
                            plugin.getMethods().getLocationManager().saveLocation(player.getLocation(), "lobby");
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        return false;
    }
}
