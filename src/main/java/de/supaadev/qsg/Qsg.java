package de.supaadev.qsg;

import de.supaadev.qsg.mysql.Configuration;
import de.supaadev.qsg.mysql.DataBaseHandler;
import de.supaadev.qsg.mysql.MySQL;
import de.supaadev.qsg.utils.Methods;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public final class Qsg extends JavaPlugin {

    public static final String PREFIX = "§6Qsg §8| §7";

    private Configuration configuration;

    private static Qsg instance;

    public Methods methods;

    private MySQL mySQL;
    private DataBaseHandler dataBaseHandler;

    @Override
    public void onEnable() {

        instance = this;
        methods = new Methods();

        mySQL = new MySQL( getConfiguration( ).getDataBaseCredential( ) );
        mySQL.connect( );
        mySQL.createTable( );
        dataBaseHandler = new DataBaseHandler( mySQL );

        // Plugin startup logic

    }


    public static Qsg getInstance( ) {
        return instance;
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
