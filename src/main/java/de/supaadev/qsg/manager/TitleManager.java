package de.supaadev.qsg.manager;

import net.minecraft.server.v1_15_R1.ChatMessageType;
import net.minecraft.server.v1_15_R1.IChatBaseComponent;
import net.minecraft.server.v1_15_R1.PacketPlayOutChat;
import org.bukkit.ChatColor;
import org.bukkit.craftbukkit.v1_15_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;

public class TitleManager {

    public static void sendTitle(Player player, Integer fadeIn, Integer stay, Integer fadeOut, String title, String subtitle) {
        try {
            if ( title != null ) {
                title = ChatColor.translateAlternateColorCodes( '&', title );

                Object e = getNMSClass( "PacketPlayOutTitle" ).getDeclaredClasses()[0].getField( "TIMES" ).get( null );

                Object chatTitle = getNMSClass( "IChatBaseComponent" ).getDeclaredClasses()[0].getMethod( "a", new Class[]{String.class} ).invoke( null, new Object[]{"{\"text\":\"" + title + "\"}"} );

                Constructor subtitleConstructor = getNMSClass( "PacketPlayOutTitle" ).getConstructor( new Class[]{
                        getNMSClass( "PacketPlayOutTitle" ).getDeclaredClasses()[0], getNMSClass( "IChatBaseComponent" ), Integer.TYPE, Integer.TYPE, Integer.TYPE} );

                Object titlePacket = subtitleConstructor.newInstance( new Object[]{e, chatTitle, fadeIn, stay, fadeOut} );

                sendPacket( player, titlePacket );

                e = getNMSClass( "PacketPlayOutTitle" ).getDeclaredClasses()[0].getField( "TITLE" ).get( null );

                chatTitle = getNMSClass( "IChatBaseComponent" ).getDeclaredClasses()[0].getMethod( "a", new Class[]{String.class} ).invoke( null, new Object[]{"{\"text\":\"" + title + "\"}"} );

                subtitleConstructor = getNMSClass( "PacketPlayOutTitle" ).getConstructor( new Class[]{
                        getNMSClass( "PacketPlayOutTitle" ).getDeclaredClasses()[0], getNMSClass( "IChatBaseComponent" )} );

                titlePacket = subtitleConstructor.newInstance( new Object[]{e, chatTitle} );

                sendPacket( player, titlePacket );
            }
            if ( subtitle != null ) {
                subtitle = ChatColor.translateAlternateColorCodes( '&', subtitle );
                Object e = getNMSClass( "PacketPlayOutTitle" ).getDeclaredClasses()[0].getField( "TIMES" ).get( null );

                Object chatSubtitle = getNMSClass( "IChatBaseComponent" ).getDeclaredClasses()[0].getMethod( "a", new Class[]{String.class} ).invoke( null, new Object[]{"{\"text\":\"" + title + "\"}"} );

                Constructor subtitleConstructor = getNMSClass( "PacketPlayOutTitle" ).getConstructor( new Class[]{
                        getNMSClass( "PacketPlayOutTitle" ).getDeclaredClasses()[0], getNMSClass( "IChatBaseComponent" ), Integer.TYPE, Integer.TYPE, Integer.TYPE} );

                Object subtitlePacket = subtitleConstructor.newInstance( new Object[]{e, chatSubtitle, fadeIn, stay, fadeOut} );

                sendPacket( player, subtitlePacket );

                e = getNMSClass( "PacketPlayOutTitle" ).getDeclaredClasses()[0].getField( "SUBTITLE" ).get( null );

                chatSubtitle = getNMSClass( "IChatBaseComponent" ).getDeclaredClasses()[0].getMethod( "a", new Class[]{String.class} ).invoke( null, new Object[]{"{\"text\":\"" + subtitle + "\"}"} );

                subtitleConstructor = getNMSClass( "PacketPlayOutTitle" ).getConstructor( new Class[]{
                        getNMSClass( "PacketPlayOutTitle" ).getDeclaredClasses()[0], getNMSClass( "IChatBaseComponent" ), Integer.TYPE, Integer.TYPE, Integer.TYPE} );

                subtitlePacket = subtitleConstructor.newInstance( new Object[]{e, chatSubtitle, fadeIn, stay, fadeOut} );

                sendPacket( player, subtitlePacket );
            }
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }
    }

    public static void sendActionBar(Player player, String Nachricht) {
        final String NachrichtNeu = Nachricht.replace( "_", " " );
        String s = ChatColor.translateAlternateColorCodes( '&', NachrichtNeu );
        IChatBaseComponent icbc = IChatBaseComponent.ChatSerializer.a( "{\"text\": \"" + s +
                "\"}" );
        PacketPlayOutChat bar = new PacketPlayOutChat();
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket( bar );
    }

    public static Class<?> getNMSClass(String name) {
        try {
            return Class.forName( "net.minecraft.server." + getVersion() + "." + name );
        } catch ( ClassNotFoundException ex ) {
            ex.printStackTrace();
        }
        return null;
    }

    public static void sendPacket(Player player, Object packet) {
        try {
            Object handle = player.getClass().getMethod( "getHandle", new Class[0] ).invoke( player, new Object[0] );
            Object playerConnection = handle.getClass().getField( "playerConnection" ).get( handle );
            playerConnection.getClass().getMethod( "sendPacket", new Class[]{
                    getNMSClass( "Packet" )} )
                    .invoke( playerConnection, new Object[]{packet} );
        } catch ( Exception ex ) {
            ex.printStackTrace();
        }
    }

    public static String getVersion() {
        return org.bukkit.Bukkit.getServer().getClass().getPackage().getName().split( "\\." )[3];
    }
}
